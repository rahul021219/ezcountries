# ezcountries

A new Flutter project.

## Assignment

1. An example of route navigation between two screens.
2. The full list of countries is displayed alphabetically.
3. A function to search for a country by country code (e.g., IN, SG, PH). There should be proper error handling and messages displayed if the user inputs a country code that doesn't exist (e.g., XY, FOO, BAR).
4. At least one method of filtering the countries (e.g., language).
