class GqlQueries {
  /// Query for Country list
  String countryGqlQuery() {
    return '''
    query {
      countries {
        code
        name
        languages {
          code
          name
        }
        emoji
      }
    }
    ''';
  }


  /// Query for language list
  String languageListGqlQuery() {
    return '''
    query {
      languages {
        name
        code
      }
    }
    ''';
  }

  /// Query for country search by country code
  String searchCountryGqlQuery({String? code}) {
    return '''
    query {
      country(code: "$code") {
        name
      }
    }
    ''';
  }
}





