import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:ezcountries/services/gql/gql_queries.dart';
import 'package:ezcountries/models/country.dart';


class Api {
  final HttpLink httpLink = HttpLink('https://countries.trevorblades.com/graphql');
  final AuthLink authLink = AuthLink(getToken: () async => "",);
  
  late Link link;
  late GraphQLClient client;

  Country? countries;
  GqlQueries queries = GqlQueries();

 Api() {
    link = authLink.concat(httpLink);
    client = GraphQLClient(link: link, cache: GraphQLCache());
  }

/// Fetch Country Data from API
  Future getCountry() async {
  final String query = queries.countryGqlQuery();
  final QueryResult result = await client.query(
    QueryOptions(
      document: gql(query),
    ),
  );

  return json.encode(result.data);
  }

  /// Fetch Language Data from API
  Future getLanguage() async {
    final String query = queries.languageListGqlQuery();
    final QueryResult result = await client.query(
      QueryOptions(
        document: gql(query),
      ),
    );
    return json.encode(result.data!['languages']);
  }


  /// Fetch Language Data from API
  Future getCountrySearchCode(context, {String? code}) async {
    final String query = queries.searchCountryGqlQuery(code: code);
    final QueryResult result = await client.query(QueryOptions(document: gql(query),),);
    if (result.hasException) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("YOu have entered wrong code!!"),));
      return null;
    }
    return json.encode(result.data);
  }
}