import 'dart:convert';
import 'package:ezcountries/models/country.dart';
import 'package:ezcountries/services/gql/gql_api.dart';
import 'package:flutter/material.dart';

class CountryProvider extends ChangeNotifier {
  Api api = Api();
  List<Country>? _countries = [];
  
  Country? _country;
  List<Country>? get countries => _countries;
  Country? get country => _country;

  List<Country> filteredCountries = [];

  final List<Language> _languages = [];
  List<Language> get languages => _languages;
  
  Future notifylistener() async {
    notifyListeners();
  }

  /// Fetching Country list in alphabetically order
  Future getCountry() async {
    final res = await api.getCountry();
    final decode = jsonDecode(res);
    final parse = Data.fromJson(decode);
    _countries = parse.countries;
    _countries?.sort((a, b) => a.name!.compareTo(b.name!));

    notifyListeners();
  }


  /// Fetching Language list
  Future getLanguageList() async {
    final result = await api.getLanguage();
    final decode = jsonDecode(result);
    for (var i in decode) {
      languages.add(Language.fromJson(i));
    }
    notifyListeners();
  }

  Future getFilteredCountryList(language,provider) async {
    List<Country> _temp = [];
    _temp.addAll(provider.countries!);
    for (var i in _temp) {
      for (var j in i.languages!) {
        if (j.name!.toLowerCase().contains(language.toLowerCase())) {
          filteredCountries.add(i);
          notifyListeners();
        }
      }
    }
  }

///Search Country by Country Code
Future getCountryByCode(context, {String? code}) async {
    final res = await api.getCountrySearchCode(context, code: code);
    if (res != null) {
      final decode = jsonDecode(res);
      if (decode['country'] == null) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Opps!! Wrong Country Code!!",
          style: Theme.of(context).textTheme.caption?.copyWith(color: Colors.white),),
          backgroundColor: Theme.of(context).colorScheme.error,
        ));
        return;
      }
      final parse = Country.fromJson(decode['country']);
      notifyListeners();
      return parse.name;
    }
  }
}

