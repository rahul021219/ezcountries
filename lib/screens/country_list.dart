import 'package:ezcountries/providers/country_provider.dart';
import 'package:ezcountries/screens/country_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class CountryList extends StatefulWidget {
  const CountryList({Key? key}) : super(key: key);

  @override
  _CountryListState createState() => _CountryListState();
}

class _CountryListState extends State<CountryList> {
  late CountryProvider provider;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () async {
      await provider.getCountry();
      await provider.getLanguageList();
    });
  }
  
  @override
  void dispose() {
    //provider.countries!.clear();
    super.dispose();
  }
  
  final _key = GlobalKey<ScaffoldState>();
  
  @override
  Widget build(BuildContext context) {
    provider = Provider.of<CountryProvider>(context);
    
    return Scaffold(
      key: _key,
      endDrawer: eZdrawer(),
      appBar: AppBar(
        centerTitle: true,
        title: const Text('eZCountry'),
        actions: [
          provider.filteredCountries.isEmpty? IconButton(
            icon: const Icon(Icons.filter_alt),
            onPressed: () {
              _key.currentState!.openEndDrawer();
            },
          )
          : IconButton(
            icon: const Icon(Icons.close_outlined),
            onPressed: () {
              provider.filteredCountries.clear();
              provider.notifylistener();
            },
          ),
        ],
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 2, 52, 61),
      ),
      
      body: Stack (
        fit: StackFit.expand,
        children: [
          provider.countries!.isEmpty? const Center(child: CupertinoActivityIndicator()): provider.filteredCountries.isEmpty ? _fullCountryList(): _filteredCountryList(),

        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        backgroundColor: const Color.fromARGB(255, 2, 52, 61),
        onPressed: () {
          Route route = MaterialPageRoute(builder: (_) => const CountrySearch());
              Navigator.push(context, route);
        },
      ),
    );
  }

/// Widget for displaying full country list
Widget _fullCountryList() {
  return ListView.builder(
    itemCount: provider.countries!.length,
    itemBuilder: (context, index) {
      final country = provider.countries![index].name;
      final language = provider.countries![index].languages;
      final emoji = provider.countries![index].emoji;
      final code = provider.countries![index].code;
      
      return Card (
        child: ListTile(
          leading: Text(emoji!),
          title: Text(country!),
          subtitle: language!.isNotEmpty? Text(language[0].name!): const Text(""),
          trailing: Text(code!),
          dense: true,
          onTap: () { /* react to the tile being tapped */ }
        ),
        elevation: 3,
        margin: const EdgeInsets.all(7),
        shape:  OutlineInputBorder(
          borderRadius: BorderRadius.circular(10), 
          borderSide: const BorderSide(color: Colors.white)
        ),
      );
    },
  );
}


/// Widget for displaying filtered country list
Widget _filteredCountryList(){
  return ListView.builder(
    itemCount: provider.filteredCountries.length,
    itemBuilder: (context, index) {
      final country = provider.filteredCountries[index].name;
      final language = provider.filteredCountries[index].languages;
      final emoji = provider.filteredCountries[index].emoji;
      final code = provider.filteredCountries[index].code;
      
      return Card (
        child: ListTile(
          leading: Text(emoji!),
          title: Text(country!),
          subtitle: language!.isNotEmpty? Text(language[0].name!): const Text(""),
          trailing: Text(code!),
          dense: true,
          onTap: () { /* react to the tile being tapped */ }
        ),
        elevation: 3,
        margin: const EdgeInsets.all(7),
        shape:  OutlineInputBorder(
          borderRadius: BorderRadius.circular(10), 
          borderSide: const BorderSide(color: Colors.white)
        ),
      );
    },
  );
}

  Drawer eZdrawer(){
    return Drawer(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: provider.languages.length,
        itemBuilder: (context, index) {
          final name = provider.languages[index].name;
          return ListTile(
            onTap: () async {
              await provider.getFilteredCountryList(name,provider);
              provider.notifylistener();
              Navigator.of(context).pop();
            },
            title: Text(name!),
          );
        }),
    );
  }
}