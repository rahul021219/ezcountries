import 'package:ezcountries/providers/country_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CountrySearch extends StatefulWidget {
  const CountrySearch({Key? key}) : super(key: key);

  @override
  _CountrySearchState createState() => _CountrySearchState();
}

class _CountrySearchState extends State<CountrySearch> {
  // late CountryProvider provider;
  final TextEditingController _controller = TextEditingController();
  String? name;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    CountryProvider provider = Provider.of<CountryProvider>(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('eZCountry'),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 2, 52, 61),
      ),
      body: Column(
        children: [
          const ListTile(
            title: Text('Country'),
            subtitle: Text('Search Here'), 
          ),
          SizedBox(
            child: TextFormField(
              controller: _controller,
              maxLength: 2,
              decoration: InputDecoration(
                labelText: 'Search your country code here!!!',
                labelStyle: const TextStyle(color: Color.fromARGB(255, 2, 52, 61),),
                suffixIcon: const Icon(Icons.check_circle,),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0),),
              ),
            ),
          ),
          ListTile(
          title: Text(name ?? ""), 
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
            selected: true,
            selectedTileColor: const Color.fromARGB(255, 14, 76, 92),
            leading: const FlutterLogo(),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: const Color.fromARGB(255, 2, 52, 61),
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                textStyle: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold)),
            onPressed: () async { 
              name = await (provider.getCountryByCode(context,
              code: _controller.text.trim().toUpperCase()));
              _controller.clear();
            },
            child: const Text('Search!',),
          ),
        ],
      ),   
    );  
  }
}
