import 'dart:convert';

CountryModel countryModelFromJson(String str) => CountryModel.fromJson(json.decode(str));

String countryModelToJson(CountryModel data) => json.encode(data.toJson());

class CountryModel {
    CountryModel({
        this.data,
    });

    Data? data;

    factory CountryModel.fromJson(Map<String, dynamic> json) => CountryModel(
        data: Data.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data!.toJson(),
    };
}

class Data {
    Data({
        this.countries,
    });

    List<Country>? countries;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        countries: List<Country>.from(json["countries"].map((x) => Country.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "countries": List<dynamic>.from(countries!.map((x) => x.toJson())),
    };
}

class Country {
    Country({
        this.code,
        this.name,
        this.languages,
        this.emoji,
    });

    String? code;
    String? name;
    List<Language>? languages;
    String? emoji;

    factory Country.fromJson(Map<String, dynamic> json) => Country(
        code: json["code"],
        name: json["name"],
        languages: json["languages"] != null ? List<Language>.from(json["languages"].map((x) => Language.fromJson(x))) : [],
        emoji: json["emoji"],
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "name": name,
        "languages": List<dynamic>.from(languages!.map((x) => x.toJson())),
        "emoji": emoji,
    };
}

class Language {
    Language({
        this.code,
        this.name,
    });

    String? code;
    String? name;

    factory Language.fromJson(Map<String, dynamic> json) => Language(
        code: json["code"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "name": name,
    };
}
