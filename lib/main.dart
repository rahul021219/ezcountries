import 'package:ezcountries/providers/country_provider.dart';
import 'package:ezcountries/screens/country_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  // debugPaintSizeEnabled = true;
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
      return MultiProvider(
        providers: [ ChangeNotifierProvider(create: (_) => CountryProvider()),], 
        child: const MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'eZCountry',
          home: CountryList(),
        ),
      );
  }
}
